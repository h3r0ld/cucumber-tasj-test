package com.tasj;

import cucumber.api.java.After;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by Herold on 01.08.2016.
 */
public class TodosStepsHooks {

    @After("@clean")
    public void clearData(){
        executeJavaScript("localStorage.clear()");
        open("http://todomvc.com");
    }
}
