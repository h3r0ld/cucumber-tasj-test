package com.tasj;

import com.codeborne.selenide.CollectionCondition;

import java.util.List;

/**
 * Created by Herold on 01.08.2016.
 */
public class CustomConditions {

    public static CollectionCondition exactTexts(List<String> expectedTexts){
        String[] texts = expectedTexts.toArray(new String[0]);
        return CollectionCondition.exactTexts(texts);
    }
}
